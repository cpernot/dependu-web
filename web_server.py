import flask as fl
from dependu import dependu
DEBUG = False
app = fl.Flask(__name__)
conf_path = dependu.get_conf_path()
dictionnary = dependu.load(conf_path)

@app.route('/', methods=['POST', 'GET'])
def web():
    result = None
    word = None
    absent_letters = None
    erreur = None
    if fl.request.method == 'POST':
        word = fl.request.form['word']
        absent_letters = fl.request.form['absents']
        for c in absent_letters:
            if c in word:
                erreur = 'Une lettre est à la fois absente et présente dans le mot (#Schrödinger)'
                result = None
                break
        else:
            result = dependu.get_result(word, absent_letters, dictionnary)

    return fl.render_template('dependu.j2', erreur=erreur, result=result, absents=absent_letters, word=word)


app.run(host='0.0.0.0', port=5000)
